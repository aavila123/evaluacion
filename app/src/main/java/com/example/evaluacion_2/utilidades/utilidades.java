package com.example.evaluacion_2.utilidades;

public class utilidades {

    public static final String tabla_pasajero="pasajeros";
    public static final String campo_id="id";
    public static final String campo_nombres="nombres";
    public static final String campo_apellidos="apellidos";
    public static final String campo_cedula="cedula";
    public static final String crear_tabla="CREATE TABLE pasajeros ("+campo_id+" TEXT, "+campo_nombres+" TEXT, "+campo_apellidos+" TEXT, "+campo_cedula+" TEXT)";

}
