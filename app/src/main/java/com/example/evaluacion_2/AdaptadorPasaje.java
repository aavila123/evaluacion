package com.example.evaluacion_2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdaptadorPasaje extends RecyclerView.Adapter<AdaptadorPasaje.ViewHolderJinetes> implements View.OnClickListener {

    ArrayList<Pasaje> listaPasaje;

    private View.OnClickListener click;

    public AdaptadorPasaje(ArrayList<Pasaje> listaPasaje) {

        this.listaPasaje = listaPasaje;
    }

    @NonNull
    @Override
    public AdaptadorPasaje.ViewHolderJinetes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);

        view.setOnClickListener(this);

        return new ViewHolderJinetes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorPasaje.ViewHolderJinetes holder, int position) {
        holder.adap_numero.setText(listaPasaje.get(position).getNumero());
        holder.adap_avion.setText(listaPasaje.get(position).getAvion());
        holder.adap_fecha_hora.setText(listaPasaje.get(position).getFecha_hora());
        holder.adap_cedula_pasajero.setText(listaPasaje.get(position).getCedula_pasajero());
        holder.adap_origen_destino.setText(listaPasaje.get(position).getOrigen_destino());


    }

    @Override
    public int getItemCount() {
        return listaPasaje.size();
    }

    public void setOnClickListener(View.OnClickListener click){
        this.click=click;
    }

    @Override
    public void onClick(View v) {
        if (click!=null){
            click.onClick(v);
        }
    }

    public class ViewHolderJinetes extends RecyclerView.ViewHolder {

        TextView adap_numero, adap_avion, adap_fecha_hora, adap_cedula_pasajero, adap_origen_destino;

        public ViewHolderJinetes(@NonNull View itemView) {
            super(itemView);
            adap_numero = (TextView)itemView.findViewById(R.id.num_pasaje);
            adap_avion = (TextView)itemView.findViewById(R.id.avion);
            adap_fecha_hora = (TextView)itemView.findViewById(R.id.fecha_hora);
            adap_cedula_pasajero = (TextView)itemView.findViewById(R.id.ced_pasajero);
            adap_origen_destino = (TextView)itemView.findViewById(R.id.origen_destino);
        }
    }
}
