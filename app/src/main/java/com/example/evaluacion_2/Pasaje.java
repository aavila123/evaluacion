package com.example.evaluacion_2;

import android.content.Intent;

public class Pasaje {

    private String numero;
    private String avion;
    private String fecha_hora;
    private String cedula_pasajero;
    private String origen_destino;

    public Pasaje(String numero, String avion, String fecha_hora, String cedula_pasajero, String origen_destino) {
        this.numero = numero;
        this.avion = avion;
        this.fecha_hora = fecha_hora;
        this.cedula_pasajero = cedula_pasajero;
        this.origen_destino = origen_destino;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAvion() {
        return avion;
    }

    public void setAvion(String avion) {
        this.avion = avion;
    }

    public String getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(String fecha_hora) {
        this.fecha_hora = fecha_hora;
    }

    public String getCedula_pasajero() {
        return cedula_pasajero;
    }

    public void setCedula_pasajero(String cedula_pasajero) {
        this.cedula_pasajero = cedula_pasajero;
    }

    public String getOrigen_destino() {
        return origen_destino;
    }

    public void setOrigen_destino(String origen_destino) {
        this.origen_destino = origen_destino;
    }
}
