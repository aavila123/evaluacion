package com.example.evaluacion_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.evaluacion_2.entidades.pasajeros;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerPasajes;

    ArrayList<Pasaje>arrayPasajes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerPasajes = (RecyclerView)findViewById(R.id.recyclerPasajes);
        recyclerPasajes.setLayoutManager(new LinearLayoutManager(this));

        arrayPasajes = new ArrayList<>();

        AdaptadorPasaje adapterJinetes = new AdaptadorPasaje(arrayPasajes);
        adapterJinetes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, detalles_pasajero.class);
                i.putExtra("caballoId",arrayPasajes.get(recyclerPasajes.getChildAdapterPosition(v)).getNumero());

                //Bundle x = new Bundle();
                //x.putString("caballoApodo",arrayJinetes.get(recyclerJinetes.getChildAdapterPosition(v)).getApodo());
                //Toast.makeText(MainActivity.this, "Se selecciono el jinete: "+arrayJinetes.get(recyclerJinetes.getChildAdapterPosition(v)).getApodo(), Toast.LENGTH_SHORT).show();
                startActivity(i);
            }
        });
        recyclerPasajes.setAdapter(adapterJinetes);



        llenarRecyclerPasajes();
    }



    private void llenarRecyclerPasajes() {
        arrayPasajes.add(new Pasaje("1","LAN","12-06-2019 / 16:00","1384759302","Manta - Quito"));
        arrayPasajes.add(new Pasaje("2","TIMES","15-08-2019 / 09:00","1394876589","Quito - Rio de Janeiro"));
        arrayPasajes.add(new Pasaje("3","LAN","19-05-2019 / 15:00","1328473948","Guayaquil - Santiago de Chile"));
    }
}
