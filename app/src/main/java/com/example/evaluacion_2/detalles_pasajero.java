package com.example.evaluacion_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evaluacion_2.utilidades.utilidades;

import org.w3c.dom.Text;

public class detalles_pasajero extends AppCompatActivity {

    String id_pasajero;
    public static final String pasajero_1="1";
    public static final String pasajero_2="2";
    public static final String pasajero_3="3";




    TextView id, nombre, apellido, cedula;


    Button consultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles_pasajero);

        id = (TextView)findViewById(R.id.id_pasajero);
        nombre = (TextView)findViewById(R.id.nom_pasajero);
        apellido = (TextView)findViewById(R.id.apellidos_pasajero);
        cedula = (TextView)findViewById(R.id.ced_pasajero);

        consultar = (Button)findViewById(R.id.btn_consultar);

        Bundle bundle = this.getIntent().getExtras();
        id_pasajero = (bundle.getString("caballoId"));

        id.setText(id_pasajero);

        if (TextUtils.equals(id_pasajero, pasajero_1)){
            llenarDatosPasajero1();

            consultar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mostrarPasajero();

                }
            });
        }


    }



    private void llenarDatosPasajero1() {

        conexion conn1 = new conexion(detalles_pasajero.this, utilidades.tabla_pasajero, null, 1);
        SQLiteDatabase db = conn1.getWritableDatabase();
        Bundle bundle = this.getIntent().getExtras();
        ContentValues values = new ContentValues();
        values.put(utilidades.campo_id,id.getText().toString());
        values.put(utilidades.campo_nombres,"Roberto Enrique");
        values.put(utilidades.campo_apellidos,"Andrade Mero");
        values.put(utilidades.campo_cedula,"1384759302");
        Toast.makeText(this, "Datos del Pasajero 1 Insertados", Toast.LENGTH_SHORT).show();
        db.close();

    }

    private void mostrarPasajero() {

        conexion x = new conexion(detalles_pasajero.this, utilidades.tabla_pasajero, null, 1);
        SQLiteDatabase db = x.getReadableDatabase();
        String[] parametros = {id.getText().toString()};

        try {

            Cursor cursor=db.rawQuery("SELECT "+utilidades.campo_nombres+","+utilidades.campo_apellidos+","+utilidades.campo_cedula+" FROM "+utilidades.tabla_pasajero+" WHERE "+utilidades.campo_id+"=? ",parametros);
            cursor.moveToFirst();
            nombre.setText(cursor.getString(0));
            apellido.setText(cursor.getString(1));
            cedula.setText(cursor.getString(2));

        }catch (Exception e){
            Toast.makeText(this, "No se puede realizar la consulta", Toast.LENGTH_SHORT).show();
        }





    }
}
